defmodule Unleash.HTTPClientAdapter do
  @moduledoc """
  Behaviour that all http clients must implement in order to
  be compatible
  """

  @type header :: {String.t(), String.t()}
  @type headers :: [header]
  @type response :: %{
          status: non_neg_integer(),
          headers: headers(),
          body: map()
        }
  @type error_reason :: any()

  @callback get(url :: String.t(), headers()) :: {:ok, response()} | {:error, error_reason()}
  @callback post(url :: String.t(), headers(), body :: String.t()) ::
              {:ok, response()} | {:error, error_reason()}
end
