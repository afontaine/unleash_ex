if Code.ensure_loaded?(Req) do
  defmodule Unleash.ReqClient do
    @moduledoc """
    Http client using Req
    """

    @behaviour Unleash.HTTPClientAdapter

    @impl true
    def get(url, headers) do
      [url: url, headers: headers]
      |> Req.new()
      |> Req.get()
      |> case do
        {:ok, response} ->
          response = %{
            status: response.status,
            headers: get_headers(response.headers),
            body: response.body
          }

          {:ok, response}

        {:error, reason} ->
          {:error, reason}
      end
    end

    @impl true
    def post(url, headers, body) do
      [url: url, headers: headers, body: body]
      |> Req.new()
      |> Req.post()
      |> case do
        {:ok, response} ->
          response = %{
            status: response.status,
            headers: get_headers(response.headers),
            body: response.body
          }

          {:ok, response}

        {:error, reason} ->
          {:error, reason}
      end
    end

    defp get_headers(headers) do
      for {k, values} <- headers,
          v <- values,
          do: {k, v}
    end
  end
end
