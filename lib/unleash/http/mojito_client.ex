if Code.ensure_loaded?(Mojito) do
  defmodule Unleash.MojitoClient do
    @moduledoc """
    Http client using Mojito
    """

    @behaviour Unleash.HTTPClientAdapter

    @impl true
    def get(url, headers) do
      url
      |> Mojito.get(headers)
      |> case do
        {:ok, response} ->
          response = %{
            status: response.status_code,
            headers: response.headers,
            body: decode_json(response)
          }

          {:ok, response}

        {:error, reason} ->
          {:error, reason}
      end
    end

    @impl true
    def post(url, headers, body) do
      url
      |> Mojito.post(headers, body)
      |> case do
        {:ok, response} ->
          response = %{
            status: response.status_code,
            headers: response.headers,
            body: decode_json(response)
          }

          {:ok, response}

        {:error, reason} ->
          {:error, reason}
      end
    end

    defp decode_json(response) do
      content_type =
        case List.keyfind(response.headers, "content-type", 0) do
          {_, value} -> value
          nil -> nil
        end

      if response.status_code == 200 and content_type == "application/json" do
        json_or_body(response.body)
      else
        response.body
      end
    end

    defp json_or_body(body) do
      case Jason.decode(body) do
        {:ok, decoded} -> decoded
        {:error, _reason} -> body
      end
    end
  end
end
