defmodule Unleash.Strategy.FlexibleRollout do
  @moduledoc """
  Can depend on `:user_id` or `:session_id` in `t:Unleash.context/0`

  Based on the
  [`flexibleRollout`](https://unleash.github.io/docs/activation_strategy#flexiblerollout)
  strategy.
  """

  use Unleash.Strategy, name: "FlexibleRollout"

  alias Unleash.Strategy.Utils

  def enabled?(%{"rollout" => percentage} = params, context) when is_number(percentage) do
    sticky_value = params |> Map.get("stickiness", "") |> stickiness(context)
    group = Map.get(params, "groupId", Map.get(params, :feature_toggle, ""))

    {
      Enum.all?([!!sticky_value, percentage > 0, Utils.normalize(sticky_value, group) <= percentage]),
      %{
        group: group,
        percentage: percentage,
        sticky_value: sticky_value,
        stickiness: Map.get(params, "stickiness")
      }
    }
  end

  def enabled?(%{"rollout" => percentage} = params, context) when is_binary(percentage) do
    enabled?(%{params | "rollout" => Utils.parse_int(percentage)}, context)
  end

  defp stickiness("userId", ctx), do: Map.get(ctx, :user_id)
  defp stickiness("sessionId", ctx), do: Map.get(ctx, :session_id)
  defp stickiness("random", _ctx), do: random()
  defp stickiness("default", ctx), do: Map.get(ctx, :user_id, Map.get(ctx, :session_id, random()))
  defp stickiness("", ctx), do: stickiness("default", ctx)
  defp stickiness(custom_name, ctx), do: get_in(ctx, [:properties, String.to_atom(Recase.to_snake(custom_name))])

  defp random, do: Integer.to_string(round(:rand.uniform() * 100) + 1)
end
