defmodule Unleash.TelemetryTest do
  @moduledoc false
  use ExUnit.Case

  def attach_telemetry_event(event) do
    :telemetry.attach(
      make_ref(),
      event,
      &__MODULE__.echo_back/4,
      test_pid: self(),
      expected_event: event
    )
  end

  def echo_back(event, measurements, metadata, opts) do
    test_pid = Keyword.fetch!(opts, :test_pid)
    expected_event = Keyword.fetch!(opts, :expected_event)

    assert expected_event == event

    send(test_pid, {:telemetry_measurements, measurements})
    send(test_pid, {:telemetry_metadata, metadata})
  end
end
