defmodule Unleash.MojitoClientTest do
  @moduledoc false
  use ExUnit.Case, async: true

  alias Unleash.MojitoClient

  describe "post/3" do
    test "returns ok result" do
      bypass = Bypass.open()
      url = url(bypass)
      headers = [{"x-request-id", "xPt0"}]
      body = "hello_world"

      Bypass.expect_once(bypass, "POST", "/", fn conn ->
        assert Plug.Conn.get_req_header(conn, "x-request-id") == ["xPt0"]
        {:ok, ^body, conn} = Plug.Conn.read_body(conn)

        conn = Plug.Conn.put_resp_header(conn, "etag", "x")
        conn = Plug.Conn.put_resp_header(conn, "content-type", "application/json")

        Plug.Conn.resp(conn, 200, ~s<{}>)
      end)

      {:ok, %{status: 200, body: %{}, headers: resp_headers}} =
        MojitoClient.post(url, headers, body)

      assert {"etag", "x"} in resp_headers
      assert {"content-type", "application/json"} in resp_headers
    end

    test "returns error" do
      bypass = Bypass.open()
      url = url(bypass)

      headers = [{"x-request-id", "xPt0"}]
      body = "hello_world"

      Bypass.down(bypass)

      {:error, %Mojito.Error{}} =
        MojitoClient.post(url, headers, body)
    end
  end

  describe "get/2" do
    test "returns ok result" do
      bypass = Bypass.open()
      url = url(bypass)
      headers = [{"x-request-id", "xPt0"}]

      Bypass.expect_once(bypass, "GET", "/", fn conn ->
        assert Plug.Conn.get_req_header(conn, "x-request-id") == ["xPt0"]

        conn = Plug.Conn.put_resp_header(conn, "etag", "x")
        conn = Plug.Conn.put_resp_header(conn, "content-type", "application/json")

        Plug.Conn.resp(conn, 200, ~s<{}>)
      end)

      {:ok, %{status: 200, body: %{}, headers: resp_headers}} =
        MojitoClient.get(url, headers)

      assert {"etag", "x"} in resp_headers
      assert {"content-type", "application/json"} in resp_headers
    end

    test "returns error" do
      bypass = Bypass.open()
      url = url(bypass)

      headers = [{"x-request-id", "xPt0"}]

      Bypass.down(bypass)

      {:error, %Mojito.Error{}} =
        MojitoClient.get(url, headers)
    end
  end

  defp url(bypass), do: "http://localhost:#{bypass.port}/"
end
